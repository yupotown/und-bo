package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{}

func handleStaticFiles(w http.ResponseWriter, r *http.Request) {
	// パスの正規化を行い、指定されたファイルがstaticディレクトリ内にあるか確認
	filePath := path.Clean("./static" + r.URL.Path)

	if !strings.HasPrefix(filePath, "static/") && filePath != "static" {
		http.NotFound(w, r)
		return
	}

	if filePath == "static/unds/list.json" {
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	}

	http.ServeFile(w, r, filePath)
}

type Client struct {
	Conn *websocket.Conn
	ID   uuid.UUID
}

var clients = map[uuid.UUID]Client{}

type PlaySoundSignal struct {
	SenderID uuid.UUID
	ID       string
}

type PongSignal struct {
	SenderID uuid.UUID
}

type Message struct {
	Type    string `json:"type"`
	Content json.RawMessage
}

type PlaySoundMessage struct {
	Type string `json:"type"`
	ID   string `json:"id"`
}

type PingMessage struct {
	Type string `json:"type"`
}

type PongMessage struct {
	Type string `json:"pong"`
}

func handleWebSocket(w http.ResponseWriter, r *http.Request) {
	var client Client

	var err error
	if client.Conn, err = upgrader.Upgrade(w, r, nil); err != nil {
		fmt.Println(err)
		return
	}
	defer client.Conn.Close()

	if client.ID, err = uuid.NewUUID(); err != nil {
		fmt.Println(err)
		return
	}

	clients[client.ID] = client
	defer delete(clients, client.ID)

	fmt.Printf("connected: %s\n", client.ID)
	defer func() {
		fmt.Printf("disconnected: %s\n", client.ID)
	}()

	for {
		var msgType struct {
			Type string `json:"type"`
		}
		_, msg, err := client.Conn.ReadMessage()
		if err != nil {
			fmt.Println(err)
			break
		}
		if err := json.Unmarshal(msg, &msgType); err != nil {
			fmt.Println(err)
			continue
		}

		switch msgType.Type {
		case "play":
			var msgPlaySound PlaySoundMessage
			if err := json.Unmarshal(msg, &msgPlaySound); err != nil {
				fmt.Println(err)
				continue
			}
			chPlaySound <- PlaySoundSignal{client.ID, msgPlaySound.ID}

		case "ping":
			var msgPing PingMessage
			if err := json.Unmarshal(msg, &msgPing); err != nil {
				fmt.Println(err)
				continue
			}
			chPong <- PongSignal{client.ID}
		}
	}
}

var chPlaySound = make(chan PlaySoundSignal)
var chPong = make(chan PongSignal)

func handlePlaySoundSignals() {
	for {
		srcMsg := <-chPlaySound
		for id, client := range clients {
			if id == srcMsg.SenderID {
				continue
			}
			destMsg, err := json.Marshal(PlaySoundMessage{"play", srcMsg.ID})
			if err != nil {
				fmt.Println(err)
				continue
			}
			client.Conn.WriteMessage(websocket.TextMessage, destMsg)
		}
	}
}

func handlePongSignals() {
	for {
		srcMsg := <-chPong
		destMsg, err := json.Marshal(PongMessage{"pong"})
		if err != nil {
			fmt.Println(err)
			continue
		}
		clients[srcMsg.SenderID].Conn.WriteMessage(websocket.TextMessage, destMsg)
	}
}

func main() {
	http.HandleFunc("/", handleStaticFiles)
	http.HandleFunc("/ws", handleWebSocket)

	go handlePlaySoundSignals()
	go handlePongSignals()

	// サーバーを起動
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	fmt.Printf("Server listening on http://localhost:%s\n", port)
	if err := http.ListenAndServe(fmt.Sprintf(":%s", port), nil); err != nil {
		fmt.Println(err)
	}
}
