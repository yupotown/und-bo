const AudioContext = window.AudioContext || window.webkitAudioContext;
const audioContext = new AudioContext();

let unds = {};
let socket;
let pingIntervalId;

// 最初に自分から再生するまでは再生しないようにするためのフラグ
let firstPlayed = false;

function show(query) {
    document.querySelector(query).style.display = 'block';
}

function hide(query) {
    document.querySelector(query).style.display = 'none';
}

function showError(message) {
    document.querySelector('#errorMessage').innerText = message;
    show('#errorMessage');
}

function showInfo(message) {
    document.querySelector('#infoMessage').innerText = message;
    show('#infoMessage');
}

function ping() {
    socket.send(JSON.stringify({'type': 'ping'}));
}

function play(id, sendsMessage = false) {
    // 最初に自分から再生するまでは再生しない
    if (!firstPlayed) {
        if (!sendsMessage) {
            return;
        }
        firstPlayed = true;

        hide('#infoMessage');
    }

    console.log('play:', id);

    const und = unds[id];
    const masterVolume = document.querySelector('#volume').value / 100;

    const audioSource = audioContext.createBufferSource();
    audioSource.buffer = und.audio;
    const gainNode = audioContext.createGain();
    gainNode.gain.value = und.volume * masterVolume;
    gainNode.connect(audioContext.destination);
    audioSource.connect(gainNode);
    audioSource.start();

    if (sendsMessage) {
        socket.send(JSON.stringify({'type': 'play', 'id': id}));
    }
}

function unlockAudio() {
    audioContext.resume();
}

function addUnd(id, iconPath, audioPath, text, volume) {
    let newPanel = document.querySelector('#panelTemplate').content.cloneNode(true);
    newPanel.querySelector('a').href = `javascript: play('${id}', true);`
    newPanel.querySelector('.panel').id = `panel_${id}`;
    newPanel.querySelector('.panel').classList.add('disabled');
    newPanel.querySelector('.panelIcon').src = iconPath;
    newPanel.querySelector('.panelText').innerText = text;
    document.querySelector('#panels').appendChild(newPanel);

    return fetch(audioPath)
    .then(res => res.arrayBuffer())
    .then(arrBuf => {
        return new Promise((resolve, reject) => {
            audioContext.decodeAudioData(arrBuf, (audioBuf) => {
                unds[id] = {
                    icon: iconPath,
                    audio: audioBuf,
                    text: text,
                    volume: volume,
                };

                document.querySelector(`#panel_${id}`).classList.remove('disabled');

                console.log('loaded:', unds[id]);

                resolve({id: unds[id]});
            }, (err) => {
                reject(err);
            })
        });
    });
}

function connect() {
    const host = window.location.hostname;
    const protocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:';
    const port = window.location.port || (protocol === 'wss:' ? '443' : '80');
    const socketUrl = `${protocol}//${host}:${port}/ws`;

    console.log('connecting:', socketUrl);
    socket = new WebSocket(socketUrl);

    showInfo('何か再生するまで、他の人が鳴らした音は再生されません。');

    fetch('./unds/list.json')
    .then(res => res.json())
    .then(list => {
        list.forEach(item => {
            addUnd(item.id, item.icon_path, item.audio_path, item.text, item.volume)
            .catch(err => {
                console.log(err);
            });
        });
    })
    .catch(_ => {
        showError('一覧の取得に失敗しました。ページをリロードしてください。');
    });

    // 接続が確立されたときの処理
    socket.addEventListener('open', (event) => {
        console.log('WebSocket connection established:', event);

        pingIntervalId = setInterval(ping, 10 * 1000);
    });

    // サーバーからメッセージを受け取ったときの処理
    socket.addEventListener('message', (event) => {
        console.log('Message from server:', event.data);

        const data = JSON.parse(event.data);
        switch (data.type) {
        case 'play':
            play(data.id);
            break;

        case 'pong':
            // do nothing
            break;
        }
    });

    // 接続が閉じられたときの処理
    socket.addEventListener('close', (event) => {
        console.log('WebSocket connection closed:', event);

        clearInterval(pingIntervalId);

        showError('サーバーとの接続が切れました。ページをリロードしてください。');
    });

    // エラーが発生したときの処理
    socket.addEventListener('error', (event) => {
        console.error('WebSocket error:', event);

        showError('通信エラーが発生しました。うまく動かない場合は、ページをリロードしてください。');
    });
}

document.addEventListener("click", unlockAudio, { once:true, evnetCapture:true });
